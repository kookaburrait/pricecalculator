﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;

namespace ICG.PriceCalculator.Core.Helpers
{
    public static class ItemFormatHelper
    {
        public static string GetFormattedCurrency(string field, decimal value)
        {
            return $"{field}: {GetFormattedCurrency(value)}";
        }
        public static string GetFormattedCurrency(decimal value)
        {
            return value.ToString("C", new CultureInfo("en-GB"));
        }
        public static string GetFormattedPercentage(decimal value)
        {
            return value.ToString("P0");
        }
    }
}
