﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ICG.PriceCalculator.Core.Models;

namespace ICG.PriceCalculator.Core.Interfaces
{
    public interface IBasketManagementService
    {
        Basket PurchaseItems(IEnumerable<string> items);
    }
}