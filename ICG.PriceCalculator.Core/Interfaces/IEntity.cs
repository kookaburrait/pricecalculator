﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ICG.PriceCalculator.Core.Interfaces
{
    public interface IEntity
    {
        int Id { get; set; }

        string Name { get; set; }
    }
}
