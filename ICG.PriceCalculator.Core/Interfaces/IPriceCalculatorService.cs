﻿using System;
using System.Collections.Generic;
using System.Text;

using ICG.PriceCalculator.Core.Models;

namespace ICG.PriceCalculator.Core.Interfaces
{
    public interface IPriceCalculatorService
    {
        decimal GetTotalPrice(Basket basket);
        decimal GetSubTotal(Basket basket);
        decimal GetItemDiscountPrice(Item item);
    }
}
