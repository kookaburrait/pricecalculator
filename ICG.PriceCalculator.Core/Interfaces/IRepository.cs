﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using ICG.PriceCalculator.Core.Models;

namespace ICG.PriceCalculator.Core.Interfaces
{
    public interface IRepository
    {
        Item Get(int id);
        IEnumerable<Item> All();
    }
}
