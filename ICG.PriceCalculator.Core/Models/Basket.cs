﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Globalization;

using ICG.PriceCalculator.Core.Helpers;

namespace ICG.PriceCalculator.Core.Models
{
    public class Basket
    {
        public ICollection<Item> Items { get; set; } = new List<Item>();
        public ICollection<string> InvalidItems { get; set; } = new List<string>();
        public decimal SubTotal { get; set; }
        public decimal TotalPrice { get; set; }
        public IEnumerable<Item> RedeemedOffers { get; set; } = new List<Item>();
        public IEnumerable<Item> ItemsWithOffers => Items.Where(x => x.SpecialOffer != null);
    }
}
