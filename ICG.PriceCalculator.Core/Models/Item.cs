﻿using System;
using System.Collections.Generic;
using System.Text;

using ICG.PriceCalculator.Core.Interfaces;

namespace ICG.PriceCalculator.Core.Models
{
    public class Item : IEntity
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public decimal Price { get; set; }

        public int Quantity { get; set; }

        public int SpecialOfferId { get; set; }

        public virtual SpecialOffer SpecialOffer { get; set; }
    }
}
