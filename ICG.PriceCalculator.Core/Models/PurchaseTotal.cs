﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ICG.PriceCalculator.Core.Models
{
    public class PurchaseTotal
    {
        public int TotalCount { get; set; }
        public int TotalQuantity { get; set; }
        public string Purchase { get; set; }
    }
}
