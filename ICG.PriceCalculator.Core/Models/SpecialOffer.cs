﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ICG.PriceCalculator.Core.Models
{
    public class SpecialOffer
    {
        public decimal PercentDiscount { get; set; }
        public int Quantity { get; set; } = 1;
        public string Purchase { get; set; }
        public bool IsMultiBuyOffer => Quantity > 1;
    }
}
