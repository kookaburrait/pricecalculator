﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

using ICG.PriceCalculator.Core;
using ICG.PriceCalculator.Core.Models;
using ICG.PriceCalculator.Core.Interfaces;
using ICG.PriceCalculator.Core.Helpers;

namespace ICG.PriceCalculator.Core.Services
{
    /// <summary>
    /// Responsible for managing basket tasks; adding items, calculating purchase prices and printing output 
    /// </summary>
    public class BasketManagementService : IBasketManagementService
    {
        private readonly IRepository _itemRepository;
        private readonly IPriceCalculatorService _priceCalculatorService;

        public BasketManagementService(IRepository itemRepository, IPriceCalculatorService priceCalculatorService)
        {
            _itemRepository = itemRepository;
            _priceCalculatorService = priceCalculatorService;
        }

        public Basket PurchaseItems(IEnumerable<string> items)
        {
            var basket = new Basket();

            AddPurchasesToBasket(items, basket);

            CalculatePurchasesInBasket(basket);

            PrintPurchaseDetails(basket);

            return basket;
        }

        private void CalculatePurchasesInBasket(Basket basket)
        {
            basket.SubTotal = _priceCalculatorService.GetSubTotal(basket);
            basket.TotalPrice = _priceCalculatorService.GetTotalPrice(basket); 
        }

        private void AddPurchasesToBasket(IEnumerable<string> items, Basket basket)
        {
            var allProducts = GetAllProducts();

            foreach (var option in items)
            {
                if (!SearchProductListForOption(allProducts, option).Any())
                {
                    // log unfound item
                    basket.InvalidItems.Add($"Unable for find item {option} in product list");
                    continue;
                }

                var item = GetItemFromProductList(allProducts, option);
                basket.Items.Add(item);
            }
        }

        private Item GetItemFromProductList(List<Item> allProducts, string option)
        {
            var lookedupItems = SearchProductListForOption(allProducts, option);
            return lookedupItems.Single();
        }

        private IEnumerable<Item> SearchProductListForOption(List<Item> allProducts, string option)
        {
            return allProducts.Where(x => x.Name.ToLower() == option.ToLower());
        }

        private List<Item> GetAllProducts()
        {
            return _itemRepository.All().ToList();
        }

        private void PrintPurchaseDetails(Basket basket)
        {
            PrintPurchaseItemValue("SubTotal", basket.SubTotal);
            PrintPurchaseDiscounts(basket);
            PrintPurchaseItemValue("Total", basket.TotalPrice);
        }

        private void PrintPurchaseItemValue(string itemName, decimal itemValue)
        {
            Console.WriteLine(ItemFormatHelper.GetFormattedCurrency(itemName, itemValue));
        }

        private void PrintPurchaseDiscounts(Basket basket)
        {
            if (!basket.RedeemedOffers.Any())
            {
                Console.WriteLine("(No Offers Available)");
                return;
            }

            foreach (var discount in basket.RedeemedOffers)
            {
                Console.WriteLine($"{discount.Name} {ItemFormatHelper.GetFormattedPercentage(discount.SpecialOffer.PercentDiscount)} off: -{ItemFormatHelper.GetFormattedCurrency(_priceCalculatorService.GetItemDiscountPrice(discount))}");
            }
        }

    }
}
