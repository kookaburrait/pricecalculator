﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using ICG.PriceCalculator.Core.Interfaces;
using ICG.PriceCalculator.Core.Helpers;
using ICG.PriceCalculator.Core.Models;

namespace ICG.PriceCalculator.Core.Services
{
    /// <summary>
    /// Responsible for basket calculations
    /// </summary>
    public class PriceCalculatorService : IPriceCalculatorService
    {
        public decimal GetTotalPrice(Basket basket)
        {
            var subtotal = GetSubTotal(basket);

            basket.RedeemedOffers = GetRedeemedOffers(basket);
            return subtotal - GetTotalDiscount(basket);
        }

        public decimal GetSubTotal(Basket basket)
        {
            return basket.Items.Sum(x => x.Price);
        }

        public decimal GetItemDiscountPrice(Item item)
        {
            return item.Price * (item.SpecialOffer != null ? item.SpecialOffer.PercentDiscount : 0);
        }

        private decimal GetTotalDiscount(Basket basket)
        {
            decimal totalDiscount = 0;
            
            foreach (var offer in basket.RedeemedOffers)
            {
                totalDiscount += GetItemDiscountPrice(offer);
            }
            return totalDiscount;
        }

        public List<Item> GetRedeemedOffers(Basket basket)
        {
            var redeemedOffers = new List<Item>();
            var singleBuyOffers = basket.ItemsWithOffers.Where(x => !x.SpecialOffer.IsMultiBuyOffer);

            // add single buy redeemed offers to basket
            redeemedOffers.AddRange(singleBuyOffers);
            var purchaseOfferTotals = GetPurchaseOfferTotals(basket);

            foreach (var offer in purchaseOfferTotals)
            {
                int countRedeemedOffers = GetCountRedeemedOffers(basket, offer);

                // add multi buy redeemed offers to basket
                redeemedOffers.AddRange(basket.ItemsWithOffers.Where(x => x.SpecialOffer.Purchase == offer.Purchase).Take(countRedeemedOffers));
            }
            return redeemedOffers;
        }

        private IEnumerable<PurchaseTotal> GetPurchaseOfferTotals(Basket basket)
        {
            return basket.ItemsWithOffers
                .Where(x => x.SpecialOffer.IsMultiBuyOffer)
                .GroupBy(x => x.SpecialOffer.Purchase)
                .Select(y => new PurchaseTotal
                {
                    Purchase = y.Key,
                    TotalCount = y.Select(x => x.Id).Count(),
                    TotalQuantity = y.Sum(x => x.SpecialOffer.Quantity)
                });
        }

        private int GetCountRedeemedOffers(Basket basket, PurchaseTotal offer)
        {
            var countMultiBuyOfferItems = basket.Items.Count(x => offer.Purchase == x.Name);

            return Convert.ToInt32(Math.Floor((Convert.ToDecimal(countMultiBuyOfferItems) / Convert.ToDecimal(offer.TotalQuantity)) * offer.TotalCount));
        }
    }
}
