﻿using Microsoft.EntityFrameworkCore;

using ICG.PriceCalculator.Core.Models;

namespace ICG.PriceCalculator.Data
{
    /// <summary>
    /// Price Calculator database
    /// </summary>
    public sealed class PriceCalculatorContext : DbContext
    {
        public PriceCalculatorContext(DbContextOptions options)
            : base(options)
        {
            // these are mutually exclusive, migrations cannot be used with EnsureCreated()
            // Database.EnsureCreated();
            Database.Migrate();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Item>()
               .HasOne(b => b.SpecialOffer);
        }


        public DbSet<Item> Items { get; set; }
        public DbSet<SpecialOffer> SpecialOffers { get; set; }
    }
}
