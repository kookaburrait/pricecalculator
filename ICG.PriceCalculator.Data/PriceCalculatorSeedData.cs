﻿using ICG.PriceCalculator.Core.Models;

using System.Collections.Generic;
using System.Linq;

namespace ICG.PriceCalculator.Data
{
    /// <summary>
    /// Initial data 
    /// </summary>
    public static class NRLStatsSeedData
    {
        public static void EnsureSeedData(this PriceCalculatorContext db)
        {
            if (!db.Items.Any())
            {
                var items = new List<Item>
                {
                    new Item
                    {
                        Name = "Apples",
                        Price = 1.0M,
                        Quantity = 1,
                        SpecialOffer = new SpecialOffer
                        {
                            PercentDiscount = 0.1M
                        }
                    },
                    new Item
                    {
                        Name = "Beans",
                        Price = 0.65M,
                        Quantity = 1
                    },
                    new Item
                    {
                        Name = "Bread",
                        Price = 0.8M,
                        Quantity = 1,
                        SpecialOffer = new SpecialOffer
                        {
                            PercentDiscount = 0.5M,
                            Quantity = 3,
                            Purchase = "Beans"
                        }
                    },
                    new Item
                    {
                        Name = "Milk",
                        Price = 1.30M,
                        Quantity = 1
                    }
                };

                db.Items.AddRange(items);
                db.SaveChanges();
            }
        }
    }
}
