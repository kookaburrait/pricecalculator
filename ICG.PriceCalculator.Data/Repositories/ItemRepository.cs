﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

using ICG.PriceCalculator.Core.Models;
using ICG.PriceCalculator.Core.Interfaces;

namespace ICG.PriceCalculator.Data.Repositories
{
    public class ItemRepository : IRepository
    {
        private readonly PriceCalculatorContext _context;

        public ItemRepository(PriceCalculatorContext context)
        {
            _context = context;
        }

        public Item Get(int id)
        {
            return _context.Items.Where(x => x.Id == id).Single();
        }

        public IEnumerable<Item> All()
        {
            return _context.Items;
        }

    }
}
