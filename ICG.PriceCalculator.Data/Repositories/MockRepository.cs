﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

using ICG.PriceCalculator.Core.Models;
using ICG.PriceCalculator.Core.Interfaces;

namespace ICG.PriceCalculator.Data.Repositories
{
    public class MockRepository : IRepository
    { 
        public Item Get(int id)
        {
            return All().Where(x => x.Id == id).Single();
        }

        public IEnumerable<Item> All()
        {
            return new List<Item>
            {
                new Item
                {
                    Name = "Apples",
                    Price = 1.0M,
                    Quantity = 1,
                    SpecialOffer = new SpecialOffer
                    {
                        PercentDiscount = 0.1M
                    }
                },
                new Item
                {
                    Name = "Beans",
                    Price = 0.65M,
                    Quantity = 1
                },
                new Item
                {
                    Name = "Bread",
                    Price = 0.8M,
                    Quantity = 1,
                    SpecialOffer = new SpecialOffer
                    {
                        PercentDiscount = 0.5M,
                        Quantity = 2,
                        Purchase = "Beans"
                    }
                },
                new Item
                {
                    Name = "Milk",
                    Price = 1.30M,
                    Quantity = 1
                }
            };
        }

    }
}
