﻿using System;
using System.Collections.Generic;
using System.Linq;

using Xunit;
using Moq;

using ICG.PriceCalculator.Core.Models;
using ICG.PriceCalculator.Core.Interfaces;
using ICG.PriceCalculator.Core.Services;

namespace ICG.PriceCalculator.UnitTests
{
    public class BasketManagementServiceUnitTests
    {
        private IBasketManagementService _basketManagementService;
        private Mock<IPriceCalculatorService> _priceCalculatorService;
        private Mock<IRepository> _repository;

        private static List<Item> GetTestItems()
        {
            return new List<Item>
            {
                new Item
                {
                    Name = "Apples",
                    Price = 1.0M,
                    Quantity = 1,
                    SpecialOffer = new SpecialOffer
                    {
                        PercentDiscount = 0.1M
                    }
                },
                new Item
                {
                    Name = "Beans",
                    Price = 0.65M,
                    Quantity = 1
                },
                new Item
                {
                    Name = "Bread",
                    Price = 0.8M,
                    Quantity = 1,
                    SpecialOffer = new SpecialOffer
                    {
                        PercentDiscount = 0.5M,
                        Quantity = 2,
                        Purchase = "Beans"
                    }
                },
                new Item
                {
                    Name = "Milk",
                    Price = 1.30M,
                    Quantity = 1
                }
            };
        }


        public BasketManagementServiceUnitTests()
        {
            _priceCalculatorService = new Mock<IPriceCalculatorService>();
            _repository = new Mock<IRepository>();
            _basketManagementService = new BasketManagementService(_repository.Object, _priceCalculatorService.Object);

            var testItems = GetTestItems();
            _repository.Setup(x => x.All()).Returns(testItems);
        }

        [Theory]
        [InlineData("Apples", "Bread")]
        [InlineData("Beans", "Bread")]

        public void GivenValidItemNames_ReturnsCorrectBasket(params string[] items)
        {
            //Arrange

            //Act
            var basket = _basketManagementService.PurchaseItems(items);

            //Assert
            Assert.NotEmpty(basket.Items);
            Assert.NotEmpty(basket.ItemsWithOffers);
            Assert.Empty(basket.InvalidItems);
        }

        [Theory]
        [InlineData("Rubbish1", "Rubbish2")]
        [InlineData("")]

        public void GivenInvalidItemNames_ReturnsCorrectBasket(params string[] items)
        {
            //Arrange

            //Act
            var basket = _basketManagementService.PurchaseItems(items);

            //Assert
            Assert.NotEmpty(basket.InvalidItems);
            Assert.Empty(basket.ItemsWithOffers);
            Assert.Empty(basket.Items);
        }

    }
}
