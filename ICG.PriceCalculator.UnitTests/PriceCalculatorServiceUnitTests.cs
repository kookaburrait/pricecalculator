﻿using System;
using System.Collections.Generic;
using System.Linq;

using Xunit;

using ICG.PriceCalculator.Core.Models;
using ICG.PriceCalculator.Core.Interfaces;
using ICG.PriceCalculator.Core.Services;

namespace ICG.PriceCalculator.UnitTests
{
    public class PriceCalculatorServiceUnitTests
    {
        private IPriceCalculatorService _priceCalculatorService;

        public PriceCalculatorServiceUnitTests()
        {
            _priceCalculatorService = new PriceCalculatorService();
        }

        private static Basket GetTestBasket()
        {
            return new Basket
            {
                Items = new List<Item>
                {
                    new Item
                    {
                        Name = "Apples",
                        Price = 1.0M,
                        Quantity = 1,
                        SpecialOffer = new SpecialOffer
                        {
                            PercentDiscount = 0.1M
                        }
                    },
                    new Item
                    {
                        Name = "Beans",
                        Price = 0.65M,
                        Quantity = 1
                    },
                    new Item
                    {
                        Name = "Bread",
                        Price = 0.8M,
                        Quantity = 1,
                        SpecialOffer = new SpecialOffer
                        {
                            PercentDiscount = 0.5M,
                            Quantity = 2,
                            Purchase = "Beans"
                        }
                    },
                    new Item
                    {
                        Name = "Milk",
                        Price = 1.30M,
                        Quantity = 1
                    }
                }
            };
        }

        [Fact]
        public void WhenBasketHasItems_ReturnsCorrectSubTotal()
        {
            //Arrange
            var basket = GetTestBasket();
            var expectedSubTotal = 3.75M;

            //Act
            var subTotal = _priceCalculatorService.GetSubTotal(basket);

            //Assert
            Assert.NotEqual(0, subTotal);
            Assert.Equal(subTotal, expectedSubTotal);
        }

        [Fact]
        public void WhenBasketHasItems_ReturnsCorrectTotalPrice()
        {
            //Arrange
            var basket = GetTestBasket();
            var expectedTotalPrice = 3.65M;

            //Act
            var totalPrice = _priceCalculatorService.GetTotalPrice(basket);

            //Assert
            Assert.NotEqual(0, totalPrice);
            Assert.Equal(totalPrice, expectedTotalPrice);
        }

        [Theory]
        [InlineData("Bread", 0.4)]
        [InlineData("Apples", 0.1)]
        public void GivenItemsWithDiscounts_ReturnsCorrectDiscountPrice(string itemName, decimal expectedDiscountPrice)
        {
            //Arrange
            var basket = GetTestBasket();

            var item = basket.ItemsWithOffers.Where(x => x.Name == itemName).Single();

            //Act
            var discountPrice = _priceCalculatorService.GetItemDiscountPrice(item);

            //Assert
            Assert.NotEqual(0, discountPrice);
            Assert.Equal(discountPrice, expectedDiscountPrice);
        }
    }
}
