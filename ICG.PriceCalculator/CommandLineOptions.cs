﻿using System;
using System.Collections.Generic;
using System.Text;

using CommandLine;

namespace ICG.PriceCalculator
{
    public sealed class CommandLineOptions
    {
        [Option('i', "items", Required = true, HelpText = "Items to add to basket.")]
        public IEnumerable<string> Items { get; set; }
    }
}
