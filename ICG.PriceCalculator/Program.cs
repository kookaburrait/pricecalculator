﻿using System;
using System.Globalization;
using System.Linq;
using System.IO;

using CommandLine;

using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Configuration;

using ICG.PriceCalculator.Core.Helpers;
using ICG.PriceCalculator.Core.Models;
using ICG.PriceCalculator.Core.Interfaces;
using ICG.PriceCalculator.Data.Repositories;
using ICG.PriceCalculator.Core.Services;
using ICG.PriceCalculator.Data;

namespace ICG.PriceCalculator
{
    public class Program
    {
        static void Main(string[] args)
        {
            Parser.Default.ParseArguments<CommandLineOptions>(args).WithParsed<CommandLineOptions>(opts => RunApplication(opts));

            Console.ReadLine();
        }

        static void RunApplication(CommandLineOptions options)
        {
            var servicesProvider = BuildDi();

            var service = servicesProvider.GetRequiredService<IBasketManagementService>();

            service.PurchaseItems(options.Items);
        }

        private static IServiceProvider BuildDi()
        {
            var services = new ServiceCollection();

            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);

            var configuration = builder.Build();

            IConfigurationSection config = configuration.GetSection("appSettings");

            services.AddOptions();

            services.Configure<AppSettings>(config);

            // swap mock out when DAL setup
            services.AddTransient<IRepository, MockRepository>();
            services.AddTransient<IPriceCalculatorService, PriceCalculatorService>();
            services.AddTransient<IBasketManagementService, BasketManagementService>();

            // seed db when db created

            var serviceProvider = services.BuildServiceProvider();

            return serviceProvider;
        }
    }
}
